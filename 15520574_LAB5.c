#include <msp430.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*
 * 15520574
    TRAN TRONG NHAN
	ghjkl
 */
int len=0, i = 0, j=0, is_str_received = 0;
char str_rec[50] = {'\n'};
void Check_Str();

void Configure_Clock(void)
{
    DCOCTL  = 0; //// Select lowest DCOx and MODx settings<
    BCSCTL1 = CALBC1_16MHZ; // Set DCO
    DCOCTL = CALDCO_16MHZ; //
}

//----------------------------------------------------------------------



void Configure_UART(void)
{

    P1SEL |= BIT1 + BIT2; // SECONDARY PERIPHERAL MODUL FUNCION ->UART
    P1SEL2 |= BIT1 + BIT2; // ........................................

    UCA0CTL1 = UCSWRST;

    UCA0CTL1 |= UCSSEL_2; // SMCLK

    UCA0MCTL = UCBRS_7; // TABLE PAGE 424

    UCA0BR0 = 0x8A; // clock 12mhz + baud rate 115200 -> UCBRx =104=0x68 TABLE PAGE 424
    UCA0BR1 = 0x00; // UCBR1=0

    UCA0CTL1 &=~UCSWRST; // **Initialize USCI state machine**

    IE2 |= UCA0RXIE; // ENABLE RX INTERRUPT

    _BIS_SR(GIE); // global enable interrupt
}

char* command;

void UARTSendString(char* commandz)
{
    i = 0;
    command = commandz;
    len = strlen(commandz);
    IE2 |= UCA0TXIE;

  while((IE2 & UCA0TXIE)==UCA0TXIE){}//check if not set
   //if set, TX interrupt is pending
}
////////////////////////////////

int tim=0;
//////////////////////////////////


void main(void)
{
   WDTCTL = WDTPW + WDTHOLD;           // Stop Watchdog
   P1DIR  |=  BIT0 + BIT6;             // P1.0-TXLED and P1.6-RXLED output
        P1OUT  &= ~BIT0 + BIT6;
   Configure_Clock();
   Configure_UART();

   UARTSendString("AT+RST\r\n");
        __delay_cycles(10000000);

        UARTSendString("AT+CWMODE=2\r\n");
        __delay_cycles(10000000);

        UARTSendString("AT+CIPMUX=1\r\n");
        __delay_cycles(10000000);

        UARTSendString("AT+CIPSERVER=1,5000\r\n");
        __delay_cycles(10000000);

        P1DIR  |=  BIT0 + BIT6;             // P1.0-TXLED and P1.6-RXLED output
        P1OUT  |= BIT0 + BIT6;


     _BIS_SR(GIE);

     while(1)
    {
        Check_Str();

    }
}


/////UARTSendString////////////
#pragma vector = USCIAB0TX_VECTOR
__interrupt void TraUART(void)
{

    if(i >= len-1)
    {
        IE2 &= ~UCA0TXIE; //neu da het du lieu can truyen thi tat che do truyen=
    }

    UCA0TXBUF = command[i++]; // truyen di ki tu thu i cua lenh AT command
}

///////RecUART///////////////////
#pragma vector = USCIAB0RX_VECTOR
__interrupt void RecUART(void)
{
    IFG2 &= ~UCA0RXIFG;
    if (UCA0RXBUF == '\n')
    {
            str_rec[i] = '\0';
            is_str_received = 1;
            i = 0   ;
     }

    else
    {
                str_rec[i] = UCA0RXBUF;
                 i++;

              }

        }


unsigned int n =0;
char copy[50];

char leng[20];
char strcp[2]={'/n'};
int num[7];
int d=0;

void Check_Str()
{
	__delay_cycles(4000000);
   int x,y, n, i;
   float result= 0;
   char str_re[2]={'\n'};
   char strcp[3]= {'\n'};


    if (is_str_received)
    {
        n = strlen(str_rec);

        if (str_rec[0]=='+')
        {
        	int i = 0;
 	        len = str_rec[7];
	        r = (len - 48);
	        if ((r != 4)||(str_rec[9]< 48)||(str_rec[9] > 57)|| (str_rec[11] < 48)||(str_rec[11]>57)|| (str_rec[12]!='?'))
	        {
	        	UARTSendString("AT+CIPSEND=0,8\r\n");
	        	__delay_cycles(4500000);


	        	 UARTSendString("NOT_OK\n\r\n");
	        	 __delay_cycles(1000000);
	        }
	        else if(str_rec[10]=='+'||str_rec[10]=='-'||str_rec[10]=='*'||str_rec[10]=='/')
	        {
	            x = str_rec[9] - 48;
	            y = str_rec[11] - 48;
          //////// phep cong
	            if (str_rec[10]=='+')
	            {
	            	result = x + y;
	            	n = 0;
	            	if(result < 10)
	            	{
	            		str_re[0] = '0';
	            		str_re[1] = result + 48;
	            	}
	            	else
	            	{
	            		str_re[0] = result / 10 + 48 ;
	            		str_re[1] = (int)result % 10 + 48 ;
	            	}

	            	UARTSendString("AT+CIPSEND=0,2\r\n");
	            	__delay_cycles(4500000);


	            	UARTSendString(str_re);
	            	__delay_cycles(1000000);
	            }

          ////// phep tru
	            else if  (str_rec[10] =='-')
	            {
	            	if (x >= y)
	            	{
	            		result = x - y;


	            		str_re[0] = '0';
	            		str_re[1] = result + 48;

	            		UARTSendString("AT+CIPSEND=0,2\r\n");
	            		__delay_cycles(4500000);


	            		UARTSendString(str_re);
	            		__delay_cycles(1000000);

	            	}
	            	else
	            	{
	            		result = y - x;
	            		str_re[0] = '-';
	            		str_re[1] = result + 48;


	            		UARTSendString("AT+CIPSEND=0,2\r\n");
	            		__delay_cycles(4500000);


	            		UARTSendString(str_re);
	            		__delay_cycles(1000000);



	            	}
	            }
	            else    if (str_rec[10]=='*')
	            		{
	            			result = x * y;
	            			n = 0;
	            			if(result < 10)
	            			{
	            				str_re[0] = '0';
	            				str_re[1] = result + 48;
	            			}
	            			else
	            			{
	            				str_re[0] = result / 10 + 48 ;
	            				str_re[1] = result - 10 + 48 ;
	            			}

	            			UARTSendString("AT+CIPSEND=0,2\r\n");
	            			__delay_cycles(4500000);


	            				UARTSendString(str_re);
	            				__delay_cycles(1000000);
	            		}
	            else if (str_rec[10]='/')
	            {
	            	if (y == 0)
	            	{
        		          UARTSendString("AT+CIPSEND=0,14\r\n");
        	              __delay_cycles(4500000);


        	              UARTSendString("Whoop! DIV 0\n\r\n");
        	               __delay_cycles(1000000);
	            	}
	            	else
	            	{
	            		result = (float)x / y;

	            		if (x % y == 0)
	            		{
	            			str_re[0] = '0';
	            			str_re[1] = result + 48;

	            			UARTSendString("AT+CIPSEND=0,2\r\n");
	            			__delay_cycles(4500000);

	            			UARTSendString(str_re);
	            			__delay_cycles(1000000);

	            		}
	            		else
	            		{
	            			result = result * 10.0 ;
	            			strcp[0] = result / 10 + 48 ;
	            			strcp[1] = ',';
	            			strcp[2] = (int)result % 10 + 48 ;

	            			UARTSendString("AT+CIPSEND=0,3\r\n");
	            			__delay_cycles(4500000);

	            			UARTSendString(strcp);
	            			__delay_cycles(1000000);



	            		}
	            	}

	            }

		    }

         else
  	       {
  	    	 UARTSendString("AT+CIPSEND=0,8\r\n");
              __delay_cycles(4500000);


              UARTSendString("NOT_OK\n\r\n");
               __delay_cycles(1000000);
  	       }

  	   memset (str_re,0,2);
      }
        memset (str_rec,0,50);
        is_str_received = 1;

    }
}

